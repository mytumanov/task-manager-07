package ru.mtumanov.tm.model;

import ru.mtumanov.tm.constant.ArgumentConstant;
import ru.mtumanov.tm.constant.CommandConstant;

public class Command {
    
    public static final Command CMD_HELP = new Command(CommandConstant.CMD_HELP, ArgumentConstant.CMD_HELP, "Show list arguments");

    public static final Command CMD_VERSION = new Command(CommandConstant.CMD_VERSION, ArgumentConstant.CMD_VERSION, "Show program version");

    public static final Command CMD_ABOUT = new Command(CommandConstant.CMD_ABOUT, ArgumentConstant.CMD_ABOUT, "Show about program");

    public static final Command CMD_INFO = new Command(CommandConstant.CMD_INFO, ArgumentConstant.CMD_INFO, "Show system info");

    public static final Command CMD_EXIT = new Command(CommandConstant.CMD_EXIT, null, "Close application");

    private String name;

    private String argument;

    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Command() {}

    public Command (String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    @Override
    public String toString() {
        String displayName = "";
        if (name != null && !name.isEmpty()) displayName += name;
        if (argument != null && !argument.isEmpty()) displayName += ", " + argument;
        if (description != null && !description.isEmpty()) displayName += ": " + description;
        return displayName;
    }

}
