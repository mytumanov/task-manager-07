package ru.mtumanov.tm;

import ru.mtumanov.tm.constant.ArgumentConstant;
import ru.mtumanov.tm.constant.CommandConstant;
import ru.mtumanov.tm.model.Command;
import ru.mtumanov.tm.util.FormatUtil;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConstant.CMD_ABOUT:
                showAbout();
            break;
            case ArgumentConstant.CMD_HELP:
                showHelp();
            break;
            case ArgumentConstant.CMD_VERSION:
                showVersion();
            break;
            case ArgumentConstant.CMD_INFO:
                showInfo();
                break;
            default:
                showArguemntError();
            break;
        }
    }

    private static void processCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case CommandConstant.CMD_ABOUT:
                showAbout();
                break;
            case CommandConstant.CMD_HELP:
                showHelp();
                break;
            case CommandConstant.CMD_VERSION:
                showVersion();
                break;
            case CommandConstant.CMD_INFO:
                showInfo();
                break;
            case CommandConstant.CMD_EXIT:
                exit();
                break;
            default:
                showCommandError();
                break;
        }
    }

    public static void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + FormatUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + FormatUtil.formatBytes(totalMemory));
        final long usageMemory = totalMemory - freeMemory;
        System.out.println("Usage memory: " +  FormatUtil.formatBytes(usageMemory));
    }

    private static void exit(){
        System.exit(0);
    }

    private static void showArguemntError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported...");
        System.exit(1);
    }

    private static void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported...");
        System.exit(1);
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Maksim Tumanov");
        System.out.println("e-mail: mytumanov@t1-consulting.ru");
        System.out.println("e-mail: MYTumanov@yandex.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.7.0");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(Command.CMD_ABOUT);
        System.out.println(Command.CMD_VERSION);
        System.out.println(Command.CMD_HELP);
        System.out.println(Command.CMD_INFO);
        System.out.println(Command.CMD_EXIT);
    }

}
